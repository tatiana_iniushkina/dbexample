package com.example.dbexample;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "userStore.db";
    private static final int DATABASE_VERSION = 1;

    static final String TABLE_NAME = "users";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_NAME = "name";
    static final String COLUMN_YEAR = "age";

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_NAME + " TEXT," +
                    COLUMN_YEAR + " INTEGER);";

    private static final String SQL_DELETE_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String SQL_INSERT = "INSERT INTO " + TABLE_NAME +
            " (" + COLUMN_NAME + ", " + COLUMN_YEAR + ") VALUES ";

    DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //создание таблицы
        db.execSQL(SQL_CREATE_TABLE);
        // добавление начальных данных
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_NAME
                + ", " + COLUMN_YEAR + ") VALUES ('Кузнецов', 1976);");
        db.execSQL(SQL_INSERT + "('Иванов', 1999);");
        db.execSQL(SQL_INSERT + "('Петров', 1986);");
        db.execSQL(SQL_INSERT + "('Сидоров', 2001);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);

    }
}
