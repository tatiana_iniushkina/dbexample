package com.example.dbexample;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class UserActivity extends AppCompatActivity {
    EditText nameEdit;
    EditText yearEdit;
    Button saveButton;
    Button delButton;

    DataBaseHelper dataBaseHelper;
    SQLiteDatabase db;
    Cursor userCursor;
    long userId = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        nameEdit = findViewById(R.id.name);
        yearEdit = findViewById(R.id.year);
        saveButton = findViewById(R.id.save_button);
        delButton = findViewById(R.id.delete_button);
        dataBaseHelper = new DataBaseHelper(this);
        db = dataBaseHelper.getWritableDatabase();
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            userId = extras.getLong("id");
        }
        if (userId > 0) {
            // получаем элемент по id из бд
            userCursor = db.rawQuery("select * from " + DataBaseHelper.TABLE_NAME + " where " +
                    DataBaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(userId)});
            userCursor.moveToFirst();
            nameEdit.setText(userCursor.getString(1));
            yearEdit.setText(String.valueOf(userCursor.getInt(2)));
            userCursor.close();
        } else {
            // скрываем кнопку удаления
            delButton.setVisibility(View.GONE);
        }
    }

    public void save(View view){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBaseHelper.COLUMN_NAME, nameEdit.getText().toString());
        contentValues.put(DataBaseHelper.COLUMN_YEAR, Integer.parseInt(yearEdit.getText().toString()));
        if (userId>0){
            db.update(DataBaseHelper.TABLE_NAME, contentValues, DataBaseHelper.COLUMN_ID + "="+String.valueOf(userId),null);
        } else {
            db.insert(DataBaseHelper.TABLE_NAME,null, contentValues);
        }
        goHome();
    }

    public void delete(View view){
        db.delete(DataBaseHelper.TABLE_NAME, "_id=?", new String[]{String.valueOf(userId)});
        goHome();
    }

    private void goHome() {
        db.close();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}
